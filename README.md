Size
=======================

[![Build Status](https://drone.gitea.com/api/badges/lunny/size/status.svg)](https://drone.gitea.com/lunny/size)  [![](http://gocover.io/_badge/gitea.com/lunny/size)](http://gocover.io/gitea.com/lunny/size)
[![](https://goreportcard.com/badge/gitea.com/lunny/size)](https://goreportcard.com/report/gitea.com/lunny/size)

Package size is a simple package to handle memory or disk size calculation or format

# Installation

```
go get github.com/lunny/size
```

# Usage

```Go
import (
    "fmt"

    . "github.com/lunny/size"
)

func main() {
    fmt.Println(10*M)

    size, _ := ParseSize("1.2K")
    fmt.Println(size)
}
```